# EGFBT Config

Currently a work in progress this package is a configuration manager.  It is designed to load a directory structure of javascript files or a javascript file in as configuration.

The idea is that configuration for complex applications should not be hard coded and thus it allows you to pull it out.  It also allows you to load multiple directories/files in order.

All files are expected to return a Javascript object that will be merged into the resulting configuraiton.
You can also get/set configuration via their dot name, for instance "resources.database.username"  or even "resources.database" to get the entire database configuration object.

You could then have a default file:

module.export = {
    resources:{
        database:{
            type: 'mysql',
            username: 'myApp'
    }
}

followed by an environment specific file:

module.export = {
    resources: {
        database:{
            password: 'myPass'
        }
    }
}

While this specific structure seems a bit odd, there is a more specific reasoning for this, that deals with the egfbt architecture of driver configurations.