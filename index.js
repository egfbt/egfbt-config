const FS = require('fs');
const PATH = require('path');
var config = {};

const resetConfig = () => {
	config = {};
}

const loadSettings = (pos, path) =>{
	if(! FS.existsSync(path)){
		throw new Error(path + ' is not a valid directory');
	}
	var stat = FS.lstatSync(path);

	if(stat.isDirectory()){
		const fdir = FS.readdirSync(path);
		for(var i = 0; i < fdir.length;i++){
			var p = (pos != '') ? pos + '.':'';
			loadSettings(p + fdir[i], path + '/' + fdir[i]);
		}
		return;
	}
	var elem = require(PATH.resolve(path));
	var parts = pos.split('.').reverse();
	for(var i = 1; i < parts.length;i++){
		var sub = elem;
		elem = {};
		elem[parts[i]] = sub;
	}
	config = deepMerge(config, elem);
}


const deepMerge = (target, merge) => {
	var res = {};
	var keys = Object.keys(target);
	for(var i = 0; i < keys.length; i++){		
		if(merge[keys[i]] === undefined || merge[keys[i]] === null){
			res[keys[i]] = target[keys[i]];
		}
		else{
			if(typeof target[keys[i]] == 'object' && typeof merge[keys[i]] == 'object'){
				res[keys[i]] = deepMerge(target[keys[i]], merge[keys[i]]);
			}
			else res[keys[i]] = merge[keys[i]];
		}
	}
	var keys = Object.keys(merge);
	for(var i = 0; i < keys.length; i++){
		if(!target[keys[i]]){
			res[keys[i]] = merge[keys[i]];
		}
	}
	return res;
}

/**
 * This is a safe function to get deep settings without exceptions.
 * @param {*} path such as settings.sub.sub.item
 */
const getSetting = (path = null) => {
	if(path == null) return config;
	var parts = path.split('.');
	var sub = config;
	for(var i = 0; i < parts.length; i++){
		sub = sub[parts[i]];
		if(sub == undefined) return undefined;
	}
	return sub;
}

const buildSetObj = (parts, value) => {
	var rObj = {};
	if(!Array.isArray(parts)){
		throw new Error(`Expected Array, got: ${typeof(parts)}`, {parts, value});
	}
	var sub = parts.shift(parts);
	if(parts.length > 0){
		rObj[sub] = buildSetObj(parts, value);
	}
	else {
		rObj[sub] = value;
	}
	return rObj;
}

const setSetting = (path, value) => {
	var parts = path.split('.');
	config = deepMerge(config, buildSetObj(parts, value));
}


/**
 * pass in a file to add to the configuration as specified
 * @param {*} path 
 */
const addConfig = (path) => { 
	loadSettings('', path)
}


const getConfig = () => {
	return config;
}

module.exports = {
	getSetting,
	setSetting,
	addConfig,
	resetConfig,
	getConfig,
}